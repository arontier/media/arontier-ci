# arontier-ci

This is a repository for Arontier CI (Corporate Identity).

## Colors

### Identity colors

| name                | RGB hex   | RGB                |
| ------------------- | --------- | ------------------ |
| Arontier Sky Blue   | `#2fa3dd` | `rgb(47,163,221)`  |
| Arontier Blue       | `#1a2881` | `rgb(26,40,129)`   |
| Arontier Gold       | `#b79d5c` | `rgb(183,157,92)`  |
| Arontier Black      | `#000000` | `rgb(0,0,0)`       |
| Arontier Gray       | `#717071` | `rgb(113,112,113)` |
| Arontier White Gray | `#c8c8c9` | `rgb(200,200,201)` |

### Variations

> Don't pick with hands. Use `variations.sass` script under `src` directory.

| name                        | RGB hex   | RGB                |
| --------------------------- | --------- | ------------------ |
| Arontier Sky Blue lighten-4 | `#82c8eb` | `rgb(130,200,235)` |
| Arontier Sky Blue lighten-3 | `#6dbfe7` | `rgb(109,191,231)` |
| Arontier Sky Blue lighten-2 | `#59b5e4` | `rgb(89,181,228)`  |
| Arontier Sky Blue lighten-1 | `#44ace0` | `rgb(68,172,224)`  |
| Arontier Sky Blue darken-1  | `#2296cf` | `rgb(34,150,207)`  |
| Arontier Sky Blue darken-2  | `#1e85b8` | `rgb(30,133,184)`  |
| Arontier Sky Blue darken-3  | `#1a74a1` | `rgb(26,116,161)`  |
| Arontier Sky Blue darken-4  | `#17648a` | `rgb(23,100,138)`  |
| Arontier Blue lighten-4     | `#4e61db` | `rgb(78,97,219)`   |
| Arontier Blue lighten-3     | `#3047d5` | `rgb(48,71,213)`   |
| Arontier Blue lighten-2     | `#263abc` | `rgb(38,58,188)`   |
| Arontier Blue lighten-1     | `#20319f` | `rgb(32,49,159)`   |
| Arontier Blue darken-1      | `#172474` | `rgb(23,36,116)`   |
| Arontier Blue darken-2      | `#152067` | `rgb(21,32,103)`   |
| Arontier Blue darken-3      | `#121c5a` | `rgb(18,28,90)`    |
| Arontier Blue darken-4      | `#10184d` | `rgb(16,24,77)`    |
| Arontier Gold lighten-4     | `#d4c49d` | `rgb(212,196,157)` |
| Arontier Gold lighten-3     | `#cdba8d` | `rgb(205,186,141)` |
| Arontier Gold lighten-2     | `#c5b17d` | `rgb(197,177,125)` |
| Arontier Gold lighten-1     | `#bea76c` | `rgb(190,167,108)` |
| Arontier Gold darken-1      | `#ac904c` | `rgb(172,144,76)`  |
| Arontier Gold darken-2      | `#998043` | `rgb(153,128,67)`  |
| Arontier Gold darken-3      | `#86703b` | `rgb(134,112,59)`  |
| Arontier Gold darken-4      | `#726033` | `rgb(114,96,51)`   |

To run `variations.sass`, do this in terminal:

```bash
mkdir variations && cd variations
npm install sass
wget https://gitlab.com/arontier/media/arontier-ci/-/raw/master/src/variations.sass
node_modules/.bin/sass variations.sass 2> ../variations.sass.out
cd .. && rm -rf variations
cat variations.sass.out
```

## Marks, logos, and signatures

> All marks, logos, and signatures shall be in AI (Adobe Illustrator) and SVG formats.

| filename                                                         | type                   | background | source filename                                        |
| ---------------------------------------------------------------- | ---------------------- |----------- | ------------------------------------------------------ |
| [arontier.light.logo.min.svg](logos/arontier.light.logo.min.svg) | logo                   | light      | [arontier.light.logo.ai](logos/arontier.light.logo.ai) |
| [arontier.light.mark.min.svg](logos/arontier.light.mark.min.svg) | mark                   | light      | [arontier.light.mark.ai](logos/arontier.light.mark.ai) |
| [arontier.light.sigh.min.svg](logos/arontier.light.sigh.min.svg) | signature (horizontal) | light      | [arontier.light.sigh.ai](logos/arontier.light.sigh.ai) |
| [arontier.light.sigv.min.svg](logos/arontier.light.sigv.min.svg) | signature (vertical)   | light      | [arontier.light.sigv.ai](logos/arontier.light.sigv.ai) |
| [arontier.dark.logo.min.svg](logos/arontier.dark.logo.min.svg)   | logo                   | dark       | [arontier.dark.logo.ai](logos/arontier.dark.logo.ai)   |
| [arontier.dark.mark.min.svg](logos/arontier.dark.mark.min.svg)   | mark                   | dark       | [arontier.dark.mark.ai](logos/arontier.dark.mark.ai)   |
| [arontier.dark.sigh.min.svg](logos/arontier.dark.sigh.min.svg)   | signature (horizontal) | dark       | [arontier.dark.sigh.ai](logos/arontier.dark.sigh.ai)   |
| [arontier.dark.sigv.min.svg](logos/arontier.dark.sigv.min.svg)   | signature (vertical)   | dark       | [arontier.dark.sigv.ai](logos/arontier.dark.sigv.ai)   |

To optimize an SVG file, do this in terminal:

```bash
mkdir optimized && cd optimized
npm install svgo

# for a file
./node_modules/.bin/svgo sample.svg -o sample.min.svg

# for all files under directory
mkdir output_dir
node_modules/.bin/svgo --input input_dir/*.svg --output output_dir
```

## Favicons

> All favicons shall be in PNG format.

| filename                                    | size    | purpose                |
| ------------------------------------------- | :-----: | ---------------------- |
| [favicon-16.png](favicons/favicon-16.png)   | 16x16   | Chrome, Safari, and IE |
| [favicon-32.png](favicons/favicon-32.png)   | 32x32   | old Chrome             |
| [favicon-57.png](favicons/favicon-57.png)   | 57x57   | iOS home screen        |
| [favicon-76.png](favicons/favicon-76.png)   | 76x76   | iPad home screen       |
| [favicon-96.png](favicons/favicon-96.png)   | 96x96   | GoogleTV               |
| [favicon-120.png](favicons/favicon-120.png) | 120x120 | iPhone retina touch    |
| [favicon-128.png](favicons/favicon-128.png) | 128x128 | Crome Web Store        |
| [favicon-152.png](favicons/favicon-152.png) | 152x152 | iPad retina touch      |
| [favicon-180.png](favicons/favicon-180.png) | 180x180 | iPhone 6 Plus          |
| [favicon-196.png](favicons/favicon-196.png) | 196x196 | Chrome for Android     |
| [favicon-512.png](favicons/favicon-512.png) | 512x512 |                        |

```bash
brew install imagemagick
magick 512.png -define icon:auto-resize=16,32,48,64,256 favicon.ico
brew uninstall imagemagick
```

## References

* Sass scale: [https://sass-lang.com/documentation/modules/color#scale](https://sass-lang.com/documentation/modules/color#scale)
* Google color picker: [https://www.google.com/search?q=color+picker](https://www.google.com/search?q=color+picker)
* Favicon cheat sheet: [https://github.com/audreyfeldroy/favicon-cheat-sheet](https://github.com/audreyfeldroy/favicon-cheat-sheet)
